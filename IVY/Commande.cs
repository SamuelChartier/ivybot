﻿using Discord;
using Discord.WebSocket;
using IVY.Objets;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IVY
{
    /// <summary>
    /// Regroupe les commandes que les utilisateurs peuvent utiliser.
    /// </summary>
    public class Commande
    {
        /// <summary>
        /// Constructeur de la classe Commande.
        /// </summary>
        public Commande() { }

        /// <summary>
        /// Permet de changer le nombre de points requis pour obtenir un rôle.
        /// </summary>
        /// <param name="commande">La commande entrée par l'utilisateur.</param>
        /// <returns>La tâche complétée.</returns>
        public async Task ChangerPointsRoles(SocketSlashCommand commande)
        {
            SocketGuildUser membre = (SocketGuildUser) commande.User;
            SocketGuildChannel salon = (SocketGuildChannel) await commande
                .GetChannelAsync();

            if (!membre.IsBot && membre.GetPermissions(salon).ManageChannel)
            {
                SocketRole role = (SocketRole) commande.Data.Options.Where(x =>
                    x.Name.Equals("role")).FirstOrDefault()?.Value;

                Int64 nombrePoints = (Int64) commande.Data.Options.Where(x =>
                x.Name.Equals("nombrepoints")).FirstOrDefault()?.Value;

                if (nombrePoints > 0)
                {
                    if (role.Id == Role.RolePositifId)
                    {
                        File.WriteAllText("..\\..\\..\\..\\IVY\\Configuration" +
                            "\\points_vip.txt", nombrePoints.ToString());
                        await commande.RespondAsync("Il faut maintenant " + nombrePoints
                            + " points pour obtenir le rôle " + role.Mention);
                    } 
                    else if (role.Id == Role.RoleAdminId)
                    {
                        File.WriteAllText("..\\..\\..\\..\\IVY\\Configuration" +
                            "\\points_admin.txt", nombrePoints.ToString());
                        await commande.RespondAsync("Il faut maintenant " + nombrePoints
                            + " points pour obtenir le rôle " + role.Mention);
                    } 
                    else
                    {
                        await commande.RespondAsync
                            ("Vous n'avez pas donné de rôle valide.");
                    }
                } 
                else
                {
                    await commande.RespondAsync(
                        "Le nombre de points entré ne doit pas être négatif.");
                }
            }
            else
            {
                await commande.RespondAsync("Vous n'avez pas les droits " +
                    "nécessaires pour utiliser cette commande.");
            }
        }

        /// <summary>
        /// Permet de changer le nombre de points de départ.
        /// </summary>
        /// <param name="commande">La commande entrée par l'utilisateur.</param>
        /// <param name="pointages">Le système de pointage.</param>
        /// <param name="pointsActuels">Le nombre de points de départ actuel.</param>
        /// <returns>La tâche complétée.</returns>
        public async Task ChangerPointsDepart(SocketSlashCommand commande,
            Pointage pointages, int pointsActuels)
        {
            SocketGuildUser membre = (SocketGuildUser) commande.User;
            SocketGuildChannel salon = (SocketGuildChannel) await commande
                .GetChannelAsync();

            if (!membre.IsBot && membre.GetPermissions(salon).ManageChannel)
            {
                Int64 nombrePoints = (Int64) commande.Data.Options.Where(x =>
                     x.Name.Equals("nombrepoints")).FirstOrDefault()?.Value;

                bool appliquerChangement = (bool) commande.Data.Options.Where(x =>
                    x.Name.Equals("appliquerchangement")).FirstOrDefault()?.Value;

                if (nombrePoints > 0)
                {
                    File.WriteAllText("..\\..\\..\\..\\IVY\\Configuration" +
                        "\\points_depart.txt", nombrePoints.ToString());

                    if (appliquerChangement)
                    {
                        String message = "";
                        if (nombrePoints - pointsActuels > 0)
                        {
                            for (int i = 0; i < pointages.ObtenirPointages().Count; i++)
                            {
                                ulong idMembre = pointages.ObtenirPointages()
                                    .ElementAt(i).Key;
                                pointages.ObtenirPointages()[idMembre] +=
                                    (int)nombrePoints - pointsActuels;
                            }
                            message = "Le nombre de points de chaque membre a été" +
                                " augmenté de " + (nombrePoints - pointsActuels);
                        }
                        else if (nombrePoints - pointsActuels < 0)
                        {
                            for (int i = 0; i < pointages.ObtenirPointages().Count; i++)
                            {
                                ulong idMembre = pointages.ObtenirPointages()
                                    .ElementAt(i).Key;
                                pointages.ObtenirPointages()[idMembre] +=
                                    (int)nombrePoints - pointsActuels;
                            }
                            message = "Le nombre de points de chaque membre a été" +
                                " diminué de " + (pointsActuels - nombrePoints);
                        }
                        else
                        {
                            await commande.RespondAsync("Le nouveau nombre de points " +
                                "est le même qu'avant. Aucun changement n'a été fait.");
                        }
                        await commande.RespondAsync(message);
                    }
                    else
                    {
                        await commande.RespondAsync("Le nombre de points de " +
                            "départ sera de " + nombrePoints + " points " +
                            "au prochain redémarrage.");
                    }
                }
                else
                {
                    await commande.RespondAsync(
                        "Le nombre de points entré ne doit pas être négatif.");
                }
            }
            else
            {
                await commande.RespondAsync("Vous n'avez pas les droits " +
                    "nécessaires pour utiliser cette commande.");
            }
        }

        /// <summary>
        /// Permet d'afficher les points d'un membre.
        /// </summary>
        /// <param name="commande">La commande entrée par l'utilisateur.</param>
        /// <param name="pointages">Le système de pointage.</param>
        /// <returns>La tâche complétée.</returns>
        public async Task AfficherPoints(SocketSlashCommand commande,
            Pointage pointages)
        {
            if (pointages.EstDansSystemePointage(commande.User.Id))
            {
                int points = pointages.ObtenirPointage(commande.User.Id);
                await commande.RespondAsync("Vous avez présentement " +
                    points + " points.");
            }
            else
            {
                await commande.RespondAsync("Vous ne faites pas partie du" +
                    " système de pointage.");
            }
        }

        /// <summary>
        /// Permet d'afficher les messages positifs du membre.
        /// </summary>
        /// <param name="commande">La commande entrée par l'utilisateur.</param>
        /// <param name="messages">Le système de messages.</param>
        /// <returns>La tâche complétée.</returns>
        public async Task AfficherMessagesPositifs(SocketSlashCommand commande,
            Message messages)
        {
            List<IUserMessage> lstmessage;
            try
            {
                 lstmessage = messages.ObtenirMessagesChanceGagnee(commande.User.Id);
            } 
            catch (KeyNotFoundException e)
            {
                lstmessage = null;
            }



            if (lstmessage != null && lstmessage.Count > 0)
            {
                string liste = "Voici les messages qui vous ont fait gagner" +
                    " des points : \n";
                foreach(IUserMessage message in lstmessage)
                {
                    liste += "\t\"" + message.Content + "\"\n"; 
                }

                await commande.RespondAsync(liste);
            } 
            else if (lstmessage?.Count <= 0)
            {
                await commande.RespondAsync("Aucun message ne vous a fait" +
                    " gagner de point.");
            } 
            else
            {
                await commande.RespondAsync("Vous ne faites pas partie du" +
                    " système de pointage.");
            }
        }

        /// <summary>
        /// Permet d'afficher les messages négatifs.
        /// </summary>
        /// <param name="commande">La commande de l'utilisateur.</param>
        /// <param name="messages">Le système de message.</param>
        /// <returns>La tâche complétée.</returns>
        public async Task AfficherMessagesNegatifs(SocketSlashCommand commande,
            Message messages)
        {
            List<IUserMessage> lstmessage;
            try
            {
                lstmessage = messages.ObtenirMessagesChancePerdue(commande.User.Id);
            } 
            catch (KeyNotFoundException e)
            {
                lstmessage = null;
            }



            if (lstmessage != null && lstmessage.Count > 0)
            {
                string liste = "Voici les messages qui vous ont fait perdre" +
                    " des points : \n";
                foreach (IUserMessage message in lstmessage)
                {
                    liste += "\t\"" + message.Content + "\"\n";
                }

                await commande.RespondAsync(liste);
            }
            else if (lstmessage?.Count <= 0)
            {
                await commande.RespondAsync("Aucun message ne vous a fait" +
                    " perdre de point.");
            }
            else
            {
                await commande.RespondAsync("Vous ne faites pas partie du" +
                    " système de pointage.");
            }
        }
    }
}
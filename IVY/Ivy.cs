﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Net;
using Discord.WebSocket;
using IVY.Objets;

namespace IVY
{
    /// <summary>
    /// La classe Ivy contrôle le programme et ses événements.
    /// </summary>
    public class Ivy
    {
        /// <summary>
        /// Représente le client grâce auquel le programme pourra communiquer 
        /// avec Discord (WebSocket).
        /// </summary>
        public DiscordSocketClient Client;

        /// <summary>
        /// Appelle la classe Membre.
        /// </summary>
        public readonly Membre Membres;

        /// <summary>
        /// Appelle la classe Message.
        /// </summary>
        public readonly Message Messages;

        /// <summary>
        /// Appelle la classe Pointage.
        /// </summary>
        public readonly Pointage Pointages;

        /// <summary>
        /// Appelle la classe Reaction.
        /// </summary>
        public readonly Reaction Reactions;

        /// <summary>
        /// Appelle la classe Role.
        /// </summary>
        public readonly Role Roles;

        /// <summary>
        /// Appelle la classe Commande.
        /// </summary>
        public readonly Commande Commande;

        /// <summary>
        /// Le nombre de points que les membres reçoivent au lancement.
        /// </summary>
        public int pointsDepart;

        /// <summary>
        /// Le nombre de points requis pour obtenir le rôle Vip.
        /// </summary>
        public int pointsVip;

        /// <summary>
        /// Le nombre de points requis pour obtenir le rôle Admin.
        /// </summary>
        public int pointsAdmin;

        /// <summary>
        /// Constructeur de la classe Ivy. Configure le DiscordSocketClient et 
        /// gère les événements Discord.
        /// </summary>
        /// <param name="token">Le token du bot associé au programme.</param>
        public Ivy(string token)
        {
            // On initialise le nouveau client Discord avec la
            // configuration donnée.
            //
            // (DiscordSocketConfig possède déjà des configurations par défaut:
            // nous n'ajoutons que celles que nous voulons de plus.)
            Client = new DiscordSocketClient(new DiscordSocketConfig()
            {
                // Nous nous assurons ici que les utilisateurs seront toujours
                // téléchargés automatiquement.
                AlwaysDownloadUsers = true,
                MessageCacheSize = 100,
                GatewayIntents = GatewayIntents.All
            });

            Membres = new();
            Messages = new();
            Pointages = new();
            Reactions = new();
            Roles = new();
            Commande = new();

            // Fonction appelée à l'entrée d'un nouveau log, et lance la
            // méthode les affichant en console.
            Client.Log += Log;

            // Fonction appelée dès qu'une guilde est disponible, et lance la
            // méthode de préparation du pointage des membres de celle-ci.
            //
            // (Le programme n'est fait que pour être utilisé sur un serveur à
            // la fois: cette fonction ne marchera pas sinon.)
            Client.GuildAvailable += Initialisation;

            // Fonction appelée dès qu'une réaction est ajoutée à un message,
            // et lance la méthode d'analyse.
            Client.ReactionAdded += ReactionDetectee;

            // Fonction appelée quand un message est supprimé afin de le
            // retirer du répertoire des messages.
            Client.MessageDeleted += SuppressionMessage;

            // Fonction appelée quand un membre est banni de la guilde afin de
            // le retirer du répertoire des membres.
            Client.UserBanned += BannissementMembre;

            // Fonction appelée quand un nouveau membre rejoint la guilde, afin
            // de l'ajouter dans le système.
            Client.UserJoined += MembreRejoint;

            // Fonction appelée quand un membre quitte la guilde, afin de le
            // retirer du système.
            Client.UserLeft += MembreQuitte;

            //Fonction appelée quand les données de la guilde ont terminé
            //d'être téléchargées.
            Client.Ready += ClientPret;

            //Fonction utilisée
            Client.SlashCommandExecuted += CommandeUtilisee;

            try
            {
                // Permet au programme de se connecter en tant que bot selon le
                // token donné avec validation.
                TokenUtils.ValidateToken(TokenType.Bot, token);
                Client.LoginAsync(TokenType.Bot, token);
            }
            catch (ArgumentException exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Appelle les bonnes méthodes selon la commande qui a été utilisée.
        /// </summary>
        /// <param name="commande">La commande utilisée</param>
        /// <returns>La tâche complétée</returns>
        private async Task CommandeUtilisee(SocketSlashCommand commande)
        {
            switch (commande.Data.Name)
            {
                case "changer-points-pour-roles":
                    await Commande.ChangerPointsRoles(commande);
                    await VerifierConfigurationAsync(
                        (SocketTextChannel) commande.Channel);
                    await VerifierPointsMembresAsync(commande);
                    break;
                case "changer-points-depart":
                    await Commande.ChangerPointsDepart(commande, Pointages,
                        pointsDepart);
                    await VerifierConfigurationAsync(
                        (SocketTextChannel) commande.Channel);
                    await VerifierPointsMembresAsync(commande);
                    break;
                case "afficher-points":
                    await Commande.AfficherPoints(commande, Pointages);
                    break;
                case "afficher-messages-positifs":
                    await Commande.AfficherMessagesPositifs(commande, Messages);
                    break;
                case "afficher-messages-negatifs":
                    await Commande.AfficherMessagesNegatifs(commande, Messages);
                    break;
            }
        }

        /// <summary>
        /// Crée les commandes qui pourront être utilisées sur la guilde.
        /// </summary>
        /// <returns>La tâche complétée.</returns>
        private async Task ClientPret()
        {
            IGuild guilde = Client.Guilds.ElementAt(0);

            List<SlashCommandBuilder> commandes = new List<SlashCommandBuilder>();

            commandes.Add(new SlashCommandBuilder()
            {
                Name = "changer-points-pour-roles",
                Description = "Permet de changer le nombre de points requis" +
                "pour obtenir une rôler"
            }
            .AddOption("role", ApplicationCommandOptionType.Role, 
            "Le rôle dont on veut changer le nombre de points.", isRequired: true)
            .AddOption("nombrepoints", ApplicationCommandOptionType.Integer,
            "Le nombre de points requis pour obtenir le rôle.", isRequired: true));

            commandes.Add(new SlashCommandBuilder()
            {
                Name = "changer-points-depart",
                Description = "Permet de changer le nombre de points que" +
                "l'on reçoit au départ."
            }
            .AddOption("nombrepoints", ApplicationCommandOptionType.Integer,
            "Le nombre de points de départ.", isRequired: true)
            .AddOption("appliquerchangement", ApplicationCommandOptionType.Boolean,
            "Si le changement doit être appliqué maintenant ou non.", isRequired: true));

            commandes.Add(new SlashCommandBuilder()
            {
                Name = "afficher-points",
                Description = "Permet d'afficher vos points."
            });

            commandes.Add(new SlashCommandBuilder()
            {
                Name = "afficher-messages-positifs",
                Description = "Permet d'afficher les messages qui vous ont" +
                " donné des points."
            });

            commandes.Add(new SlashCommandBuilder()
            {
                Name = "afficher-messages-negatifs",
                Description = "Permet d'afficher les messages qui vous ont" +
                " retiré des points."
            });

            try
            {
                foreach (SlashCommandBuilder commande in commandes)
                {
                    await guilde.CreateApplicationCommandAsync(commande.Build());
                }
            }
            catch (HttpException exception)
            {
                Console.WriteLine(exception.Errors);
            }
        }

        /// <summary>
        /// Ajoute toutes les informations dans les répertoires sur le membre
        /// qui a rejoint la guilde.
        /// </summary>
        /// <param name="membre">Le membre qui a rejoint.</param>
        /// <returns>La tâche complétée.</returns>
        private Task MembreRejoint(SocketGuildUser membre)
        {
            Console.WriteLine($"Membre ajouté {membre.Username}");
            IGuildUser nouveauMembre = membre as IGuildUser;
            Membres.AjouterMembre(nouveauMembre);
            Pointages.AjouterMembrePointage(nouveauMembre, pointsDepart);
            Messages.AjouterNouveauMembreMessages(nouveauMembre.Id);
            Roles.AjouterMembreRole(nouveauMembre.Id);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Sort toutes les informations dans les répertoires sur le membre 
        /// banni de la guilde.
        /// </summary>
        /// <param name="membre">Le membre banni.</param>
        /// <param name="guilde">La guilde d'où le membre est banni.</param>
        /// <returns>La tâche complétée.</returns>
        private Task BannissementMembre(SocketUser membre, SocketGuild guilde)
        {
            guilde.DefaultChannel.SendMessageAsync(
                $"{membre.Mention} a été banni pour mauvaise conduite. " +
                $"Au nom des responsables de la communauté {guilde.Name}, " +
                $"merci de garder une attitude positive! ❤️");
            MembreQuitte(guilde, membre);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Sort toutes les informations dans les répertoires sur le membre
        /// qui a quitté la guilde.
        /// </summary>
        /// <param name="membre">Le membre qui a quitté.</param>
        /// <param name="guilde">La guilde que le membre a quitté.</param>
        /// <returns>La tâche complétée.</returns>
        private Task MembreQuitte(SocketGuild guilde, SocketUser membre)
        {
            if (!membre.IsBot && !membre.IsWebhook && membre.Id != guilde.OwnerId)
            {
                Pointages.RetirerMembrePointage(membre.Id);
                Messages.ViderMessagesChanceGagnee(membre.Id);
                Messages.ViderMessagesChancePerdue(membre.Id);
                Messages.RetirerMembreMessages(membre.Id);
                Roles.RetirerMembreRole(membre.Id);
                Membres.RetirerMembre(Membres.ObtenirMembre(membre.Id));
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Permet au programme de se lancer.
        /// (Pour le lancement depuis les tests.)
        /// </summary>
        public void Lancement()
        {
            Client.StartAsync();
        }

        /// <summary>
        /// Permet au programme de se lancer avec .Wait().
        /// (Pour le lancement normal depuis Program.cs.)
        /// </summary>
        public void LancementEnAttente()
        {
            Client.StartAsync().Wait();
        }

        /// <summary>
        /// Permet d'afficher textuellement les logs du programme en console.
        /// (Sert de substitut rudimentaire à un logging framework.)
        /// </summary>
        /// <param name="msg">Le log entrant à afficher en console.</param>
        /// <returns>La tâche complétée.</returns>
        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        /// <summary>
        /// Lorsqu'un utilisateur retire un message, on s'assure qui soit 
        /// retiré des listes des chances gagnées/perdues si nécessaire.
        /// </summary>
        /// <param name="messageSupprime">Le message supprimé.</param>
        /// <param name="salon">
        /// Le salon du message supprimé. (Requis pour Client.MessageDeleted.)
        /// </param>
        /// <returns>La tâche complétée.</returns>
        private async Task<Task> SuppressionMessage(
                                    Cacheable<IMessage, ulong> messageSupprime,
                                    Cacheable<IMessageChannel, ulong> salon)
        {
            var message = await messageSupprime.GetOrDownloadAsync();

            if (message != null)
            {
                ulong auteurId = message.Author.Id;

                // On vérifie que le message vient d'un utilisateur faisant partie
                // du système de pointage avant de poursuivre.
                if (message != null && Pointages.EstDansSystemePointage(auteurId))
                {
                    //Si la liste n'est pas nulle, on vérifie si le message a fait
                    //gagner une chance à son auteur dans le passé et on le retire
                    //de la liste.
                    if (Messages.ObtenirMessagesChanceGagnee(auteurId) != null)
                    {
                        foreach (var msgPositif in
                           Messages.ObtenirMessagesChanceGagnee(auteurId))
                        {
                            if (message.Id == msgPositif.Id)
                            {
                                Messages.RetirerMessageChanceGagnee(msgPositif);
                            }
                        }
                    }
                    // Si la liste n'est pas nulle, on vérifie si le message a fait
                    // perdre une chance à son auteur dans le passé et le retire de
                    // la liste.
                    if (Messages.ObtenirMessagesChancePerdue(auteurId) != null)
                    {
                        foreach (var msgNegatif in
                           Messages.ObtenirMessagesChancePerdue(auteurId))
                        {
                            if (message.Id == msgNegatif.Id)
                            {
                                Messages.RetirerMessageChancePerdue(msgNegatif);
                            }
                        }
                    }
                }
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Boucle sur tous les membres de la guilde donnée afin d'ajouter les 
        /// membres "normaux" au système de pointage.
        /// (Le pointage initiale est de 5 chances.)
        /// </summary>
        /// <param name="guilde">La guilde à laquel appartient le bot.</param>
        /// <returns>La tâche complétée.</returns>
        private async Task<Task> Initialisation(SocketGuild guilde)
        {
            await VerifierConfigurationAsync(guilde.DefaultChannel);

            await foreach (var membres in guilde.GetUsersAsync())
            {
                // On ne veut pas ajouter le propriétaire de la guilde au
                // pointage, ni les webhooks et les bots.
                var membresEligibles = membres.Where(
                    m => !m.IsBot && !m.IsWebhook && m.Id != guilde.OwnerId);

                // Si la liste contient des membres éligibles, on les ajoutes à
                // la liste des membres modérés.
                if (membresEligibles.Any())
                {
                    foreach (var membre in membresEligibles)
                    {
                        Membres.AjouterMembre(membre);
                        Pointages.AjouterMembrePointage(membre, pointsDepart);
                        Messages.AjouterNouveauMembreMessages(membre.Id);
                        Roles.AjouterMembreRole(membre.Id);
                    }
                }
            }

            // Si le répertoire des membres à modérer est vide, on lance
            // l'avertissement.
            if (!Membres.ObtenirMembres().Any())
            {
                await guilde.DefaultChannel.SendMessageAsync("**Attention: " +
                    "aucun membre à modérer est actuellement détecté!**\n" +
                    "(Rappel: le propriétaire de la guilde, les webhooks " +
                    "ainsi que les bots ne sont pas modérés.)");
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// La méthode permet de faire l'analyse de la réaction et déterminer 
        /// si elle a un impacte ou non sur le pointage de l'auteur du message.
        /// </summary>
        /// <param name="messageVise">Le message visé par la réaction.</param>
        /// <param name="salonVise">
        /// Le salon textuel du message visé par une nouvelle réaction. 
        /// (Nécessaire pour la fonction Client.ReactionAdded.)
        /// </param>
        /// <param name="reaction">La nouvelle réaction détectée.</param>
        /// <returns>La tâche complétée.</returns>
        private async Task<Task> ReactionDetectee(
                                   Cacheable<IUserMessage, ulong> messageVise, 
                                   Cacheable<IMessageChannel, ulong> salonVise, 
                                   SocketReaction reaction)       
        {
            var message = await messageVise.GetOrDownloadAsync();

            // Si le message n'est pas null (comme dans l'éventualité d'un
            // message d'ajout d'un nouveau membre).
            if (message != null)
            {
                ulong auteurId = (await messageVise.GetOrDownloadAsync()).Author.Id;
                IGuildUser auteur = Membres.ObtenirMembre(auteurId);

                // Si l'auteur du message fait partie du système de pointage et
                // n'est pas en exclusion.
                if (Pointages.EstDansSystemePointage(auteurId) && 
                    auteur.TimedOutUntil.HasValue.Equals(false))
                {
                    // S'il s'agit d'une appréciation et que le message n'a pas
                    // déjà affecté les gains de chances de l'auteur.
                    if (Reactions.EstAppreciation(reaction.Emote.Name) && 
                        !(Messages.EstMessageChanceGagnee(message)))
                    {
                        int nbAppreciations = 
                            await message
                            .GetReactionUsersAsync(
                                Reactions.ObtenirAppreciation(), 5)
                            .CountAsync();
                        if (nbAppreciations == 5)
                        {
                            Pointages.AugmenterPointage(auteurId);
                            Messages.AjouterMessageChanceGagnee(message);
                            IRole role;
                            //On regarde le nombre de point du membre pour voir
                            //si un rôle est à ajouter ou retirer.
                            int pointsAuteur = Pointages.ObtenirPointage(auteurId);
                            if (pointsAuteur >= pointsDepart &&
                                auteur.RoleIds.Contains(Role.RoleNegatifId))
                            {
                                role = auteur.Guild.GetRole(Role.RoleNegatifId);
                                Roles.RetirerRole(auteur, role);
                                await auteur.RemoveRoleAsync(role);
                            }
                            if (pointsAuteur >= pointsVip && 
                                !auteur.RoleIds.Contains(Role.RolePositifId))
                            {
                                role = auteur.Guild.GetRole(Role.RolePositifId);
                                Roles.AjouterRole(auteur, role);
                                await auteur.AddRoleAsync(role);
                            }
                            if (pointsAuteur >= pointsAdmin &&
                                !auteur.RoleIds.Contains(Role.RoleAdminId))
                            {
                                role = auteur.Guild.GetRole(Role.RoleAdminId);
                                Roles.AjouterRole(auteur, role);
                                await auteur.AddRoleAsync(role);
                            }
                        }
                    }
                    // S'il s'agit d'une plainte et que le message n'a pas déjà
                    // affecté les pertes de chances de l'auteur.
                    else if (Reactions.EstPlainte(reaction.Emote.Name) && 
                        !(Messages.EstMessageChancePerdue(message)))
                    {
                        int nbPlaintes = await message
                                        .GetReactionUsersAsync(
                                                 Reactions.ObtenirPlainte(), 5)
                                        .CountAsync();
                        if (nbPlaintes == 5)
                        {
                            Pointages.DiminuerPointage(auteurId);
                            Messages.AjouterMessageChancePerdue(message);
                            IRole role;
                            //On regarde le nombre de point du membre pour voir
                            //si un rôle est à ajouter ou retirer.
                            int pointsAuteur = Pointages.ObtenirPointage(auteurId);

                            if (pointsAuteur <= 0)
                            {
                                await Suspension(auteur, salonVise.Value);
                            }
                            if (pointsAuteur < pointsVip &&
                                auteur.RoleIds.Contains(Role.RolePositifId))
                            {
                                role = auteur.Guild.GetRole(Role.RolePositifId);
                                Roles.RetirerRole(auteur, role);
                                await auteur.RemoveRoleAsync(role);
                            }
                            if (pointsAuteur < pointsAdmin &&
                                auteur.RoleIds.Contains(Role.RoleAdminId))
                            {
                                role = auteur.Guild.GetRole(Role.RoleAdminId);
                                Roles.RetirerRole(auteur, role);
                                await auteur.RemoveRoleAsync(role);
                            }
                        }
                    }
                }
            }
            return Task.CompletedTask;
        }

        /// <summary>
        /// Suspends l'utilisateur problématique et nettoie les listes et les 
        /// messages causant problèmes.
        /// </summary>
        /// <param name="suspendu">L'utilisateur à suspendre.</param>
        /// <param name="salonVise">Le dernier salon accédé par le suspendu.</param>
        /// <returns>La tâche complétée.</returns>
        public Task Suspension(IGuildUser suspendu, IMessageChannel salonVise)
        {
            // Si l'utilisateur a des messages encore problématiques.
            // (Il pourrait en avoir supprimé...)
            if (Messages.ObtenirMessagesChancePerdue(suspendu.Id) != null)
            {
                // On supprime tous les messages problématiques de
                // l'utilisateur.
                foreach (var message in 
                        Messages.ObtenirMessagesChancePerdue(suspendu.Id))
                {
                    // Si le message existe aussi dans les chances obtenues,
                    // on le retire.
                    if (Messages.ObtenirMessagesChanceGagnee(suspendu.Id)
                        .Contains(message))
                    {
                        Messages.RetirerMessageChanceGagnee(message);
                    }
                    message.DeleteAsync();
                }
                // On vide finalement la liste des messages problématiques de
                // l'auteur qui ont été détectés par le programme depuis sont
                // lancement.
                Messages.ViderMessagesChancePerdue(suspendu.Id);
            }

            // On suspend l'utilisateur puis on lui donne une chance.
            Membres.SuspendreMembre(suspendu.Id);
            Pointages.AugmenterPointage(suspendu.Id);
            IRole role = Client.Guilds.ElementAt(0).GetRole(Role.RoleNegatifId);
            if (role != null)
            {
                Roles.AjouterRole(suspendu, role);
            }
            salonVise.SendMessageAsync(
                $"{suspendu.Mention} a été suspendu pour mauvaise conduite. " +
                $"N'oubliez pas qu'une bonne attitude est nécessaire pour le " +
                $"bien de la communauté! ❤️");
            return Task.CompletedTask;
        }

        /// <summary>
        /// Vérifie les fichiers de configurations pour savoir les pointages
        /// nécessaires pour les rôles et le pointage de départ.
        /// </summary>
        /// <param name="salon">Le salon dans lequel afficher les erreurs.</param>
        /// <returns>La tâche complétée</returns>
        public async Task VerifierConfigurationAsync(SocketTextChannel salon)
        {
            string pointsAdminString = File.ReadAllText("..\\..\\..\\.." +
                "\\IVY\\Configuration\\points_admin.txt");
            string pointsVipString = File.ReadAllText("..\\..\\..\\.." +
                "\\IVY\\Configuration\\points_vip.txt");
            string pointsDepartString = File.ReadAllText("..\\..\\..\\.." +
                "\\IVY\\Configuration\\points_depart.txt");

            int pointsAdmin;
            int pointsVip;
            int pointsDepart;

            if (int.TryParse(pointsAdminString, out pointsAdmin))
            {
                this.pointsAdmin = pointsAdmin;
            } 
            else
            {
                await salon.SendMessageAsync("** Attention :" +
                    " le pointage nécessaire pour le rôle d'administrateur " +
                    "est mal configuré. Utilisez la commande " +
                    "/changer-points-pour-roles avec le rôle administrateur.**");
                this.pointsAdmin = 15;
            }
            if (int.TryParse(pointsVipString, out pointsVip))
            {
                this.pointsVip = pointsVip;
            }
            else
            {
                await salon.SendMessageAsync
                    ("** Attention : le pointage nécessaire pour le rôle" +
                    " Vip est mal configuré. Utilisez la commande " +
                "/changer-points-pour-roles avec le rôle Vip.**");
                this.pointsVip = 10;
            }
            if (int.TryParse(pointsDepartString, out pointsDepart))
            {
                this.pointsDepart = pointsDepart;
            }
            else
            {
                await salon.SendMessageAsync(
                    "** Attention : le pointage de départ est mal configuré." +
                    " Utilisez la commande /changer-points-depart.**");
                this.pointsDepart = 5;
            }
        }

        /// <summary>
        /// Vérifie les points des membres et leur applique les changements
        /// nécessaire au besoin.
        /// </summary>
        /// <param name="commande">La commande qui a causé la vérification.</param>
        /// <returns>La tâche complétée.</returns>
        public async Task VerifierPointsMembresAsync(SocketSlashCommand commande)
        {
            foreach(KeyValuePair<ulong, int> pointage in Pointages.ObtenirPointages())
            {
                SocketGuildChannel salon = (SocketGuildChannel) commande.Channel;
                await salon.Guild.DownloadUsersAsync();
                SocketGuildUser membre = salon.Guild.GetUser(pointage.Key);
                
                // Regarde si le membre a plus ou moins de points que le rôle admin.
                if (pointage.Value >= pointsAdmin && membre.Roles.Any(x =>
                    x.Id != Role.RoleAdminId))
                {
                    await membre.AddRoleAsync(Role.RoleAdminId);
                } 
                else if (membre.Roles.Any(x => x.Id == Role.RoleAdminId))
                {
                    await membre.RemoveRoleAsync(Role.RoleAdminId);
                }
                // Regarde si le membre a plus ou moins de points que le rôle vip.
                if (pointage.Value >= pointsVip && membre.Roles.Any(x =>
                    x.Id != Role.RolePositifId))
                {
                    await membre.AddRoleAsync(Role.RolePositifId);
                } 
                else if (membre.Roles.Any(x => x.Id == Role.RolePositifId))
                {
                    await membre.RemoveRoleAsync(Role.RolePositifId);
                }
                // Regarde si le membre a plus de poits que le nombre de départ.
                if (pointage.Value > pointsDepart && membre.Roles.Any(x =>
                    x.Id == Role.RoleNegatifId))
                {
                    await membre.RemoveRoleAsync(Role.RoleNegatifId);
                }
                // Regarde si le membre a perdu tout ses points.
                if (pointage.Value <= 0)
                {
                    await Suspension(membre, commande.Channel);
                }
            }
        }
    }
}

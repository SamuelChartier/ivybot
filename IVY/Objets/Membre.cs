﻿using Discord;
using System;
using IVY.Repertoires;

namespace IVY.Objets
{
    /// <summary>
    /// Représente un membre et sa gestion.
    /// </summary>
    public class Membre: RepertoireMembres
    {
        /// <summary>
        /// Constructeur de la classe Membre.
        /// </summary>
        public Membre() { }

        /// <summary>
        /// Permet d'obtenir un membre de la guilde selon l'id donné.
        /// </summary>
        /// <param name="id">L'id du membre à obtenir.</param>
        /// <returns>Le membre correspondant à l'id donné.</returns>
        public IGuildUser ObtenirMembre(ulong id)
        {
            return Membres.Find(m => m.Id.Equals(id));
        }

        /// <summary>
        /// Permet d'ajouter un membre à la liste de tous les membres.
        /// </summary>
        /// <param name="membre">Le nouveau membre à ajouter à la liste.</param>
        public void AjouterMembre(IGuildUser membre)
        {
            Membres.Add(membre);
        }

        /// <summary>
        /// Permet de retirer un membre de la liste.
        /// </summary>
        /// <param name="membre">Le membre à retirer</param>
        public void RetirerMembre(IGuildUser membre)
        {
            Membres.Remove(membre);
        }

        /// <summary>
        /// Permet de suspendre un membre pendant 24 heures.
        /// </summary>
        /// <param name="membreId">L'id du membre à suspendre.</param>
        public void SuspendreMembre(ulong membreId)
        {
            ObtenirMembre(membreId).SetTimeOutAsync((DateTime.Now.AddDays(1)) -
                DateTime.Now);
        }
    }
}

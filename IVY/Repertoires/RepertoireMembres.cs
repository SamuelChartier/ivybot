﻿using System.Collections.Generic;
using Discord;

namespace IVY.Repertoires
{
    /// <summary>
    /// Regroupe les membres de la guilde éligibles à la modération.
    /// </summary>
    public class RepertoireMembres
    {
        /// <summary>
        /// Liste tous les membres de la guilde éligibles à la modération.
        /// </summary>
        protected List<IGuildUser> Membres = new();

        /// <summary>
        /// Constructeur de la classe RepertoireMembres.
        /// </summary>
        public RepertoireMembres() { }

        /// <summary>
        /// Permet d'obtenir tous les membres éligibles de la guilde.
        /// </summary>
        /// <returns>La liste des membres éligibles de la guilde.</returns>
        public List<IGuildUser> ObtenirMembres()
        {
            return Membres;
        }

    }
}

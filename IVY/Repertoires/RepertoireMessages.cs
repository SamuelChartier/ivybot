﻿using System.Collections.Generic;
using Discord;

namespace IVY.Repertoires
{
    /// <summary>
    /// Regroupe tous les messages gérés par la modération.
    /// </summary>
    public class RepertoireMessages
    {
        /// <summary>
        /// Contient l'id des messages ayant déjà fait gagner une chance à son auteur.
        /// TKey (ulong): contient l'id de l'auteur.
        /// TValue (List<IUserMessage>): contient la liste de tous les messages ayant 
        /// donné une chance à son auteur.
        /// </summary>
        protected Dictionary<ulong, List<IUserMessage>> MessagesChanceGagnee = new();

        /// <summary>
        /// Contient l'id des messages ayant déjà fait perdre une chance à son auteur.
        /// TKey (ulong): contient l'id de l'auteur.
        /// TValue (List<IUserMessage>): contient la liste de tous les messages ayant 
        /// retiré une chance à son auteur.
        /// </summary>
        protected Dictionary<ulong, List<IUserMessage>> MessagesChancePerdue = new();

        /// <summary>
        /// Constructeur de la classe RepertoireMessages.
        /// </summary>
        public RepertoireMessages() { }

        /// <summary>
        /// Permet d'obtenir la liste de tous les messages qui ont fait gagner une 
        /// chance à leur auteur.
        /// </summary>
        /// <param name="auteurId">L'auteur des messages listés.</param>
        /// <returns>
        /// La liste de tous les messages qui ont fait gagner un chance à leur auteur.
        /// </returns>
        public List<IUserMessage> ObtenirMessagesChanceGagnee(ulong auteurId)
        {
            return MessagesChanceGagnee[auteurId];
        }

        /// <summary>
        /// Permet d'obtenir la liste de tous les messages qui ont coûté un chance 
        /// à leur auteur.
        /// </summary>
        /// <param name="auteurId">L'auteur des messages listés.</param>
        /// <returns>
        /// La liste de tous les messages qui ont coûté un chance à leur auteur.
        /// </returns>
        public List<IUserMessage> ObtenirMessagesChancePerdue(ulong auteurId)
        {
            return MessagesChancePerdue[auteurId];
        }

        /// <summary>
        /// Permet de vider la liste de tous les messages ayant fait gagner une 
        /// chance à son auteur.
        /// </summary>
        /// <param name="auteurId">
        /// L'id de l'auteur des messages dont la liste est à vider.
        /// </param>
        public void ViderMessagesChanceGagnee(ulong auteurId)
        {
            MessagesChanceGagnee[auteurId].Clear();
        }

        /// <summary>
        /// Permet de vider la liste de tous les messages ayant fait perdre une 
        /// chance à son auteur.
        /// </summary>
        /// <param name="auteurId">
        /// L'id de l'auteur des messages dont la liste est à vider.
        /// </param>
        public void ViderMessagesChancePerdue(ulong auteurId)
        {
            MessagesChancePerdue[auteurId].Clear();
        }
    }
}

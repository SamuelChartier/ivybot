﻿using System.Collections.Generic;

namespace IVY.Repertoires
{
    /// <summary>
    /// Regroupe les pointages gérés par la modération.
    /// </summary>
    public class RepertoirePointages
    {
        /// <summary>
        /// Contient le pointage des membres du serveur donné.
        /// TKey (ulong): représente l'id du membre.
        /// TValue (int): représente le pointage du membre.
        /// </summary>
        protected Dictionary<ulong, int> Pointages = new();

        /// <summary>
        /// Constructeur de la classe RepertoirePointages.
        /// </summary>
        public RepertoirePointages() { }

        /// <summary>
        /// Permet d'obtenir la liste de tous les pointages.
        /// </summary>
        /// <returns>La liste de tous les pointages.</returns>
        public Dictionary<ulong, int> ObtenirPointages()
        {
            return Pointages;
        }
    }
}

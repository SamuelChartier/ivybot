﻿using Discord;
using System.Collections.Generic;
using System.Linq;

namespace IVY.Repertoires
{
    /// <summary>
    /// Regroupe les rôles gérés par la modération.
    /// </summary>
    public class RepertoireRoles
    {
        /// <summary>
        /// Contient les rôles des membres du serveur donné.
        /// TKey (ulong): représente l'id du membre.
        /// TValue (List<IRole>): représente les rôles du membre.
        /// </summary>
        protected Dictionary<ulong, List<IRole>> Roles = new();

        /// <summary>
        /// Constructeur de la classe RepertoireRoles.
        /// </summary>
        public RepertoireRoles() { }

        /// <summary>
        /// Permet d'obtenir la liste de tous les rôles attribués à un membre.
        /// </summary>
        /// <param name="membreId">L'id du membre.</param>
        /// <returns>La liste des rôles du membre.</returns>
        public List<IRole> ObtenirRoles(ulong membreId)
        {
            return Roles[membreId];
        }

        /// <summary>
        /// Permet de vider la liste des rôles attribués à un membre.
        /// </summary>
        /// <param name="membreId">
        /// L'id du membre dont la liste est à vider.
        /// </param>
        public void ViderRoles(ulong membreId)
        {
            Roles[membreId].Clear();
        }
    }
}

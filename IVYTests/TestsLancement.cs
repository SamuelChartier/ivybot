using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace IVYTests
{
    /// <summary>
    /// Classe de tests concernant le lancement d'IVY.
    /// </summary>
    [TestClass]
    public class TestsLancement
    {
        /// <summary>
        /// Teste le constructeur pour le lancement du projet dans le cas d'un 
        /// succ�s avec l'acc�s au fichier valide.
        /// </summary>
        [TestMethod]
        public void TestConstructeurSucces()
        {
            // On teste ici avec le bon chemin et bon nom de fichier.
            StreamReader sr = new("C:\\TokenBot.txt");
            var token = sr.ReadLine();
            IVY.Ivy bot = new(token);
        }

        /// <summary>
        /// Teste le constructeur pour le lancement du projet dans le cas d'une 
        /// faute de chemin du fichier.
        /// </summary>
        [TestMethod]
        public void TestConstructeurCheminFichierInvalide()
        {
            try
            {
                IVY.Ivy bot = new("G:\\TokenBot.txt");
                Assert.Fail();
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le constructeur pour le lancement du projet dans le cas d'une 
        /// faute de nom de fichier.
        /// </summary>
        [TestMethod]
        public void TestConstructeurNomFichierInvalide()
        {
            try
            {
                IVY.Ivy bot = new("C:\\Token.txt");
                Assert.Fail();
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le constructeur pour le lancement du projet dans le cas d'une 
        /// entr�e nulle.
        /// </summary>
        [TestMethod]
        public void TestConstructeurFichierNull()
        {
            try
            {
                IVY.Ivy bot = new(null);
                Assert.Fail();
            }
            catch (ArgumentException) { }
        }
    }
}

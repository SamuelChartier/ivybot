﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using IVY.Objets;
using Moq;

namespace IVYTests
{
    /// <summary>
    /// Classe de tests concernant les messages.
    /// </summary>
    [TestClass]
    public class TestsMessages
    {
        #region Tests sur ObtenirMessagesChanceGagnee()

        /// <summary>
        /// Teste que la méthode retourne tous les messages ayant donnés une 
        /// chance à l'auteur donné.
        /// </summary>
        [TestMethod]
        public void TestObtenirMessagesChanceGagneeSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage message1 = new Mock<IUserMessage>(utilisateur.Id).Object;
                IUserMessage message2 = new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                message.AjouterNouveauMembreMessages(utilisateur.Id);
                message.AjouterMessageChanceGagnee(message1);
                message.AjouterMessageChanceGagnee(message2);
                Assert.IsTrue(message.ObtenirMessagesChanceGagnee
                    (utilisateur.Id).Count == 2);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une 
        /// requête avec un utilisateur inconnu au système en entrée.
        /// </summary>
        [TestMethod]
        public void TestObtenirMessagesChanceGagneeEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Message message = new();
                Action action = () => message.ObtenirMessagesChanceGagnee
                (utilisateur.Id);
                Assert.ThrowsException<KeyNotFoundException>(action);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une valeur
        /// nulle en entrée.
        /// </summary>
        [TestMethod]
        public void TestObtenirMessagesChanceGagneeNull()
        {
            try
            {
                Message message = new();
                int? x = null;
                Action action = () => message.ObtenirMessagesChanceGagnee((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur ObtenirMessagesChancePerdue()

        /// <summary>
        /// Teste que la méthode retourne tous les messages ayant coûtés une 
        /// chance à l'auteur donné.
        /// </summary>
        [TestMethod]
        public void TestObtenirMessagesChancePerdueSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage message1 = new Mock<IUserMessage>(utilisateur.Id).Object;
                IUserMessage message2 = new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                message.AjouterNouveauMembreMessages(utilisateur.Id);
                message.AjouterMessageChancePerdue(message1);
                message.AjouterMessageChancePerdue(message2);
                Assert.IsTrue(message.ObtenirMessagesChancePerdue
                    (utilisateur.Id).Count == 2);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une 
        /// requête avec un utilisateur inconnu au système en entrée.
        /// </summary>
        [TestMethod]
        public void TestObtenirMessagesChancePerdueEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Message message = new();
                Action action = () => message.ObtenirMessagesChancePerdue
                (utilisateur.Id);
                Assert.ThrowsException<KeyNotFoundException>(action);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une valeur
        /// nulle en entrée.
        /// </summary>
        [TestMethod]
        public void TestObtenirMessagesChancePerdueNull()
        {
            try
            {
                Message message = new();
                int? x = null;
                Action action = () => message.ObtenirMessagesChancePerdue((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur ViderMessagesChanceGagnee()

        /// <summary>
        /// Teste que la méthode vide tous les messages ayant donnés une 
        /// chance à l'auteur donné.
        /// </summary>
        [TestMethod]
        public void TestViderMessagesChanceGagneeSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage message1 = new Mock<IUserMessage>(utilisateur.Id).Object;
                IUserMessage message2 = new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                message.AjouterNouveauMembreMessages(utilisateur.Id);
                message.AjouterMessageChanceGagnee(message1);
                message.AjouterMessageChanceGagnee(message2);
                Assert.IsTrue(message.ObtenirMessagesChanceGagnee
                    (utilisateur.Id).Count == 2);
                message.ViderMessagesChanceGagnee(utilisateur.Id);
                Assert.IsTrue(message.ObtenirMessagesChanceGagnee
                    (utilisateur.Id).Count == 0);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une 
        /// requête avec un utilisateur inconnu au système en entrée.
        /// </summary>
        [TestMethod]
        public void TestViderMessagesChanceGagneeEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Message message = new();
                Action action = () => message.ViderMessagesChanceGagnee(utilisateur.Id);
                Assert.ThrowsException<KeyNotFoundException>(action);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une valeur
        /// nulle en entrée.
        /// </summary>
        [TestMethod]
        public void TestViderMessagesChanceGagneeNull()
        {
            try
            {
                Message message = new();
                int? x = null;
                Action action = () => message.ViderMessagesChanceGagnee((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur ViderMessagesChancePerdue()

        /// <summary>
        /// Teste que la méthode vide tous les messages ayant coûtés une 
        /// chance à l'auteur donné.
        /// </summary>
        [TestMethod]
        public void TestViderMessagesChancePerdueSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage message1 = new Mock<IUserMessage>(utilisateur.Id).Object;
                IUserMessage message2 = new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                message.AjouterNouveauMembreMessages(utilisateur.Id);
                message.AjouterMessageChancePerdue(message1);
                message.AjouterMessageChancePerdue(message2);
                Assert.IsTrue(message.ObtenirMessagesChancePerdue
                    (utilisateur.Id).Count == 2);
                message.ViderMessagesChancePerdue(utilisateur.Id);
                Assert.IsTrue(message.ObtenirMessagesChancePerdue
                    (utilisateur.Id).Count == 0);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une 
        /// requête avec un utilisateur inconnu au système en entrée.
        /// </summary>
        [TestMethod]
        public void TestViderMessagesChancePerdueEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                Message message = new();
                Action action = () => message.ViderMessagesChancePerdue(utilisateur.Id);
                Assert.ThrowsException<KeyNotFoundException>(action);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une valeur
        /// nulle en entrée.
        /// </summary>
        [TestMethod]
        public void TestViderMessagesChancePerdueNull()
        {
            try
            {
                Message message = new();
                int? x = null;
                Action action = () => message.ViderMessagesChancePerdue((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur AjouterMessageChanceGagnee()

        /// <summary>
        /// Teste que la méthode ajoute un nouveau message ayant donné une 
        /// chance à l'auteur donné.
        /// </summary>
        [TestMethod]
        public void TestAjouterMessageChanceGagneeSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                message.AjouterNouveauMembreMessages(utilisateur.Id);
                Assert.IsTrue(message.ObtenirMessagesChanceGagnee
                    (utilisateur.Id).Count == 0);
                message.AjouterMessageChanceGagnee(messageUtilisateur);
                Assert.IsTrue(message.ObtenirMessagesChanceGagnee
                    (utilisateur.Id).Count == 1);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une 
        /// requête avec un utilisateur inconnu au système en entrée.
        /// </summary>
        [TestMethod]
        public void TestAjouterMessageChanceGagneeEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                Action action = () => message.AjouterMessageChanceGagnee
                    (messageUtilisateur);
                Assert.ThrowsException<KeyNotFoundException>(action);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une valeur
        /// nulle en entrée.
        /// </summary>
        [TestMethod]
        public void TestAjouterMessageChanceGagneeNull()
        {
            try
            {
                Message message = new();
                Action action = () => message.AjouterMessageChanceGagnee(null);
                Assert.ThrowsException<NullReferenceException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur AjouterMessageChancePerdue()

        /// <summary>
        /// Teste que la méthode ajoute un nouveau message ayant coûté une 
        /// chance à l'auteur donné.
        /// </summary>
        [TestMethod]
        public void TestAjouterMessageChancePerdueSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                message.AjouterNouveauMembreMessages(utilisateur.Id);
                Assert.IsTrue(message.ObtenirMessagesChancePerdue
                    (utilisateur.Id).Count == 0);
                message.AjouterMessageChancePerdue(messageUtilisateur);
                Assert.IsTrue(message.ObtenirMessagesChancePerdue
                    (utilisateur.Id).Count == 1);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une 
        /// requête avec un utilisateur inconnu au système en entrée.
        /// </summary>
        [TestMethod]
        public void TestAjouterMessageChancePerdueEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                Action action = () => message.AjouterMessageChancePerdue
                    (messageUtilisateur);
                Assert.ThrowsException<KeyNotFoundException>(action);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une valeur
        /// nulle en entrée.
        /// </summary>
        [TestMethod]
        public void TestAjouterMessageChancePerdueNull()
        {
            try
            {
                Message message = new();
                Action action = () => message.AjouterMessageChancePerdue(null);
                Assert.ThrowsException<NullReferenceException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur RetirerMessageChanceGagnee()

        /// <summary>
        /// Teste que la méthode retire un message ayant donné une 
        /// chance à l'auteur donné du message lui-même.
        /// </summary>
        [TestMethod]
        public void TestRetirerMessageChanceGagneeSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                message.AjouterNouveauMembreMessages(utilisateur.Id);
                message.AjouterMessageChanceGagnee(messageUtilisateur);
                Assert.IsTrue(message.ObtenirMessagesChanceGagnee
                    (utilisateur.Id).Count == 1);
                message.RetirerMessageChanceGagnee(messageUtilisateur);
                Assert.IsTrue(message.ObtenirMessagesChanceGagnee
                    (utilisateur.Id).Count == 0);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une 
        /// requête avec le message d'un utilisateur inconnu au système en entrée.
        /// </summary>
        [TestMethod]
        public void TestRetirerMessageChanceGagneeEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                Action action = () => message.RetirerMessageChanceGagnee
                    (messageUtilisateur);
                Assert.ThrowsException<KeyNotFoundException>(action);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une valeur
        /// nulle en entrée.
        /// </summary>
        [TestMethod]
        public void TestRetirerMessageChanceGagneeNull()
        {
            try
            {
                Message message = new();
                Action action = () => message.RetirerMessageChanceGagnee(null);
                Assert.ThrowsException<NullReferenceException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur RetirerMessageChancePerdue()

        /// <summary>
        /// Teste que la méthode retire un message ayant coûté une 
        /// chance à l'auteur donné du message lui-même.
        /// </summary>
        [TestMethod]
        public void TestRetirerMessageChancePerdueSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                message.AjouterNouveauMembreMessages(utilisateur.Id);
                message.AjouterMessageChancePerdue(messageUtilisateur);
                Assert.IsTrue(message.ObtenirMessagesChancePerdue
                    (utilisateur.Id).Count == 1);
                message.RetirerMessageChancePerdue(messageUtilisateur);
                Assert.IsTrue(message.ObtenirMessagesChancePerdue
                    (utilisateur.Id).Count == 0);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une 
        /// requête avec le message d'un utilisateur inconnu au système en entrée.
        /// </summary>
        [TestMethod]
        public void TestRetirerMessageChancePerdueEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                Action action = () => message.RetirerMessageChancePerdue
                    (messageUtilisateur);
                Assert.ThrowsException<KeyNotFoundException>(action);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une valeur
        /// nulle en entrée.
        /// </summary>
        [TestMethod]
        public void TestRetirerMessageChancePerdueNull()
        {
            try
            {
                Message message = new();
                Action action = () => message.RetirerMessageChancePerdue(null);
                Assert.ThrowsException<NullReferenceException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur AjouterNouveauMembreMessages()

        /// <summary>
        /// Teste que la méthode retire un membre au système.
        /// </summary>
        [TestMethod]
        public void TestAjouterNouveauMembreMessagesSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                message.AjouterNouveauMembreMessages(utilisateur.Id);
                Assert.IsTrue(message.ObtenirMessagesChanceGagnee
                    (utilisateur.Id).Count == 0);
                message.RetirerMembreMessages(utilisateur.Id);
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une valeur
        /// nulle en entrée.
        /// </summary>
        [TestMethod]
        public void TestAjouterNouveauMembreMessagesNull()
        {
            try
            {
                Message message = new();
                int? x = null;
                Action action = () => message.AjouterNouveauMembreMessages((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur RetirerMembreMessages()

        /// <summary>
        /// Teste que la méthode retire un membre du système.
        /// </summary>
        [TestMethod]
        public void TestRetirerMembreMessagesSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                message.AjouterNouveauMembreMessages(utilisateur.Id);
                Assert.IsTrue(message.ObtenirMessagesChanceGagnee
                    (utilisateur.Id).Count == 0);
                message.RetirerMembreMessages(utilisateur.Id);
                Action action = () => message.ObtenirMessagesChanceGagnee
                    (utilisateur.Id);
                Assert.Fail(action.ToString());
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une valeur
        /// nulle en entrée.
        /// </summary>
        [TestMethod]
        public void TestRetirerMembreMessagesNull()
        {
            try
            {
                Message message = new();
                int? x = null;
                Action action = () => message.RetirerMembreMessages((ulong)x);
                Assert.ThrowsException<InvalidOperationException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

        #region Tests sur EstMessageChanceGagnee()



        #endregion

        #region Tests sur EstMessageChancePerdue()

        /// <summary>
        /// Teste que la méthode retourne true à la bonne entrée d'un
        /// utilisateur connu du système.
        /// </summary>
        [TestMethod]
        public void TestEstMessageChancePerdueSucces()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                message.AjouterNouveauMembreMessages(utilisateur.Id);
                message.AjouterMessageChancePerdue(messageUtilisateur);
                Assert.IsTrue(message.EstMessageChancePerdue(messageUtilisateur));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une 
        /// requête avec le message d'un utilisateur inconnu au système en entrée.
        /// </summary>
        [TestMethod]
        public void TestEstMessageChancePerdueEchec()
        {
            try
            {
                IGuildUser utilisateur = new Mock<IGuildUser>().Object;
                IUserMessage messageUtilisateur = 
                    new Mock<IUserMessage>(utilisateur.Id).Object;
                Message message = new();
                Assert.IsFalse(message.EstMessageChancePerdue(messageUtilisateur));
            }
            catch (ArgumentException) { }
        }

        /// <summary>
        /// Teste le comportement de la méthode lorsqu'on lui envoie une valeur
        /// nulle en entrée.
        /// </summary>
        [TestMethod]
        public void TestEstMessageChancePerdueNull()
        {
            try
            {
                Message message = new();
                Action action = () => message.EstMessageChancePerdue(null);
                Assert.ThrowsException<NullReferenceException>(action);
            }
            catch (ArgumentException) { }
        }

        #endregion

    }
}
